###### Howto use

* configure your targets in `inventories/staging/hosts`

* install&run 3 nodes zookeeper cluster with `ansible-playbook` :

```
$ ansible-playbook -i inventories/staging/hosts -K zookeepers.yml --diff
```

* check your cluster status :

```
$ ansible -i inventories/staging/hosts local-zookeepers --become -K -m shell -a '/opt/zookeeper/current/bin/zkServer.sh status'
```

